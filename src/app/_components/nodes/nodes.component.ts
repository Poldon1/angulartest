import { Component, OnInit } from '@angular/core';
import { NodesService } from '../../_services/nodes.service';

@Component({
  selector: 'app-nodes',
  templateUrl: './nodes.component.html',
  styleUrls: ['./nodes.component.css'],
  providers: [NodesService]
})
export class NodesComponent implements OnInit {
  public data: any = null;
  public newData: any = null;
  public title: string = null;
  constructor(private nodeService: NodesService) { }

  ngOnInit() {
  }

  /**
   * function to get nodes from virtual server.
   * @param url: String url.
   * @param title: String title to display in result content.
   */
  private getNodes(url: string, title: string) {
    this.title = title;

    this.nodeService.getNodesByServiceSpecification(url).subscribe(
      (response) => {
          if (response.nodes === undefined && response.edges
            === undefined) {
            alert('empty response');

            return;
          }

          if (response.nodes.length < 1 && response.edges.length < 1) {
            alert('empty nodes');

            return;
          }

          this.data = response;
          console.log(this.data);
          this.orderDiagram(this.data.nodes);
      },
      (error) => {
        console.log(error);
        alert('error geting information from virtual server');
      }
    );
  }

  /**
   * Private function for order diagram.
   * @param data:Object nodes.
   */
  private orderDiagram(data: any) {
    const newData = []
        , newOrder = [];

    let wasGateway = false;

    data.forEach(element => {
      if (element.type !== 'ServiceTask') {
        newData.push(element);
      }
    });

    for (let index = 0; index <= newData.length - 1; index++) {
      if (newData[index].type !== 'Gateway' && newData[index].type !== 'End') {
        if (wasGateway) {
          newOrder.push({from: newData[index].id, to: newData[index + 2].id});
        } else {
          newOrder.push({from: newData[index].id, to: newData[index + 1].id});
        }

        wasGateway = false;
      } else if (newData[index].type === 'Gateway') {
        newOrder.push({from: newData[index].id, to: newData[index + 1].id});

        if (newData[index + 2] !== undefined) {
          newOrder.push({from: newData[index].id, to: newData[index + 2].id});
        }

        wasGateway = true;
      }
    }

    this.newData = {nodes: newData, edges: newOrder};
  }
}
