import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NodesService } from './nodes.service';

describe('NodesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        { provide: NodesService }
      ]
    });
  });

  it('should be created', inject([HttpClientModule, NodesService], (service: NodesService) => {
    expect(service).toBeTruthy();
  }));
});
