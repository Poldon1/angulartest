import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class NodesService {
  constructor(private http: HttpClient) { }

  /**
   * get nodes by service specification.
   * @param data:String service path.
   * @returns Observable.
   */
  public getNodesByServiceSpecification(data: string): Observable<any> {
    const path = environment.serviceUrl + '/data/' + data;

    return new Observable<any>(
      observer => {
        return this.http.get(path).subscribe(
          (response) => {
            observer.next(response);
          },
          (error: any) => {
            observer.error(error);
          }
        );
      }
    );
  }
}
